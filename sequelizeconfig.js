module.exports = {
    development: {
        use_env_variable: 'DB_CONNECTION_STRING',
        dialect: 'postgres',
        freezeTableName: true,
        logging: false,
    },
    stage: {
        use_env_variable: 'DB_CONNECTION_STRING',
        dialect: 'postgres',
        freezeTableName: true,
        logging: false,
    },
    production: {
        use_env_variable: 'DB_CONNECTION_STRING',
        dialect: 'postgres',
        freezeTableName: true,
        logging: false,
    },
};
