'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.STRING(30),
                primaryKey: true,
                allowNull: false,
                unique: true
            },
            username: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
            blocked: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            emailVerified: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
                field: 'email_verified'
            },
            emailVerifiedAt: {
                type: Sequelize.DATE,
                allowNull: true,
                field: 'email_verified_at'
            },
            phoneNumber: {
                type: Sequelize.STRING,
                allowNull: true,
                field: 'phone_number'
            },
            phoneVerified: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
                field: 'phone_verified'
            },
            phoneVerifiedAt: {
                type: Sequelize.DATE,
                allowNull: true,
                field: 'phone_verified_at'
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('users');
    }
};
