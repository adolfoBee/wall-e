'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('tokens', {
            id: {
                type: Sequelize.STRING(30),
                primaryKey: true,
                allowNull: false,
                unique: true,
            },
            userId: {
                type: Sequelize.STRING,
                allowNull: false,
                field: 'user_id',
                references: {
                    model: 'users',
                    key: 'id'
                }
            },
            tokenType: {
                type: Sequelize.STRING,
                allowNull: true,
                field: 'token_type'
            },
            token: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            tokenExpirity: {
                type: Sequelize.DATE,
                allowNull: true,
                field: 'token_expirity'
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('tokens');
    }
};
