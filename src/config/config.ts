export default {
    port: process.env.PORT || 3000,
    authentication: {
        jwtSecret: process.env.JWT_SECRET,
        oneWeek: 60 * 60 * 24 * 7,
        session: 'session'
    },
    sendgridTemplates: {
        forgotPassword: 'd-bfa65adcd2d64b9d8ba04c0d4794ff41'
    },
    tokenTypes: [
        {
            name: 'pin',
            expirity: Date.now() + 1800000, //30 minutes
            length: 6
        },
        {
            name: 'session',
            expirity: Date.now() + 1800000, //30 minutes
            length: 16
        },
        {
            name: 'confirmLink',
            expirity: Date.now() + 1800000, //30 minutes
            length: 16
        }
    ]
};
