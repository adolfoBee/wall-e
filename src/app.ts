require('dotenv').config();
import _express from 'express';
import _bodyParser from 'body-parser';
// import _cors from 'cors';
import _logger from './utils/logger';
import _usersRoutes from './api/routes/users';
import _tokensRoutes from './api/routes/tokens';
import _config from './config/config';
// import './mercurios_setup';

const app = _express();

app.use(_bodyParser.urlencoded({ extended: false }));
app.use(_bodyParser.json());
// app.use(cors());

// Routes to handle requests
app.use('/users', _usersRoutes);
app.use('/tokens', _tokensRoutes);

// Error handling
app.use((req, res, next) => {
    const error: any = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({ error: { message: error.message } });
    next();
});

process
    .on('unhandledRejection', (reason, promise) => {
        console.log(reason, 'Unhandled Rejection at Promise', promise);
    })
    .on('uncaughtException', error => {
        _logger.error(new Error(`Uncaught Exception thrown - ${error.message}`));
        process.exit(1);
    });

app.listen(_config.port, () => {
    _logger.info(`Server started on port ${_config.port}`);
});

export default app;
