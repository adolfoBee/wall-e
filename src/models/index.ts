/** @todo: clean up */
import _fs from 'fs';
import _path from 'path';
import { Sequelize } from 'sequelize';
import _logger from '../utils/logger';

const basename = _path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require('../../sequelizeconfig.js')[env];
const db: any = {};

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable] as string, config);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

_fs.readdirSync(__dirname)
    .filter((file) => {
        const included = file !== basename && ['.js', '.ts'].includes(_path.extname(file));
        if (included) {
            _logger.info(`Sequialize: attaching model in ${file}`);
        }

        return included;
    })
    .forEach((file) => {
        const model = sequelize['import'](_path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
