import random from '../utils/random';
const { generateModelRandomId } = random;

export default (sequelize, DataTypes): any => {
    const tokens = sequelize.define(
        'tokens',
        {
            id: {
                type: DataTypes.STRING(30),
                primaryKey: true,
                allowNull: false,
                unique: true,
                defaultValue: function () {
                    return generateModelRandomId('tokens');
                }
            },
            userId: {
                type: DataTypes.STRING,
                allowNull: false,
                field: 'user_id'
            },
            tokenType: {
                type: DataTypes.STRING,
                allowNull: false,
                field: 'token_type'
            },
            token: {
                type: DataTypes.STRING,
                allowNull: false
            },
            tokenExpirity: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'token_expirity'
            }
        },
        {
            timestamps: true
        }
    );

    tokens.associate = (models): any => {
        tokens.template = tokens.belongsTo(models.users, {
            foreignKey: 'userId',
        });
    };

    return tokens;
};
