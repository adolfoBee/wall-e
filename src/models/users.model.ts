export default (sequelize, DataTypes): any => {
    const users = sequelize.define(
        'users',
        {
            id: {
                type: DataTypes.STRING(30),
                allowNull: false,
                primaryKey: true,
                unique: true
            },
            username: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            blocked: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                validate: {
                    isEmail: true
                }
            },
            emailVerified: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
                field: 'email_verified'
            },
            emailVerifiedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'email_verified_at'
            },
            phoneNumber: {
                type: DataTypes.STRING,
                allowNull: true,
                field: 'phone_number'
            },
            phoneVerified: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
                field: 'phone_verified'
            },
            phoneVerifiedAt: {
                type: DataTypes.DATE,
                allowNull: true,
                field: 'phone_verified_at'
            },
        },
        {
            timestamps: true
        }
    );

    users.associate = (models): any => {
        users.tokens = users.hasMany(models.tokens, { foreignKey: 'userId' });
    };

    return users;
};
