import express from 'express';
const router = express.Router();

import _usersController from '../controllers/users';

router.post('/signup', _usersController.signup);
router.post('/login', _usersController.login);
router.post('/forgotPassword', _usersController.forgotPassword);
router.post('/resetPassword', _usersController.resetPassword);

export default router;
