import express from 'express';
const router = express.Router();

import _tokensController from '../controllers/tokens';

router.post('/verifyToken', _tokensController.verifyToken);
router.post('/generateConfirmationToken', _tokensController.generateConfirmationToken);

export default router;
