import _jwt from 'jsonwebtoken';
import _bcrypt from 'bcrypt';
import _moment from 'moment';
import _logger from '../../utils/logger';
import _config from '../../config/config';
import _random from '../../utils/random';
import _db from '../../models';
import _sendGrid from '../../clients/sendgrid';
import _tokensController from './tokens';

const { users } = _db;
const oneWeek = _config.authentication.oneWeek;

function signToken(payload, expiration) {
    return _jwt.sign(payload, _config.authentication.jwtSecret, { expiresIn: expiration });
}

async function hashPassword(pwd) {
    const hashedPassword = await _bcrypt.hash(pwd, 12);
    return hashedPassword;
}

const getUser = async (userId?: string, email?: string) => {
    const whereClause = {};

    if (userId && userId !== '') {
        whereClause['id'] = userId;
    }

    if (email && email !== '') {
        whereClause['email'] = email;
    }

    return await users.findOne({ where: whereClause, raw: true });
};

const forgotPassword = async (req, res) => {
    try {
        // Checks if email exists
        _logger.debug('[Forgot Password] Checking if email exists');
        const { email } = req.body;
        if (!email || email === '') {
            throw new Error('No email was provided');
        }

        // Finds user with email
        _logger.debug('[Forgot Password] Finding user');
        const user = await getUser(undefined, email);
        if (!user || user.length < 1) {
            throw new Error('The email doesn\'t exists');
        }

        const token = await _tokensController.generateToken(user.id, 'pin');

        // Generates message for the email
        _logger.debug('[Forgot Password] Generating message user');
        const message = {
            'from': {
                'name': 'Moovinit',
                'email': 'no-reply@moovinit.com'
            },
            'personalizations': [{
                'to': [{
                    'email': email,
                }],
                'dynamic_template_data': {
                    'sender_name': 'Moovinit',
                    'sender_email': 'test@moovinit.com',
                    'sender_address': 'Durocher',
                    'sender_city': 'Montreal',
                    'sender_country': 'Canada',
                    'username': user.username,
                    'pin': token.token,
                    'request_date': _moment(token.createdAt).format('YYYY-MM-DD'),
                    'request_location': 'test_location'
                },
                'subject': 'Reset Password has been requested'
            }],
            'attachments': [{
                filename: 'moovInitLogo.png',
                type : 'image/png',
                content: 'iVBORw0KGgoAAAANSUhEUgAAAfUAAADQCAYAAAAeXvggAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAGu5JREFUeNrsnc+PHMd1x8sUIRNJaEwgwSGFIG5eBAGOwSGUgxIE5uwlDoIYmkUOCXzZ2UsQ+LLcv2C1fwGX11w4vDi+BFzBOQS5cORDkovAFWwDhC7sk8xDBG3AHARdnH7L1+Jwtbvzqqe6u6r68wEaqx/zq+vX971Xr147BwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ3yLJgAAyJpxdU30nxfVdUSTIOoAAJCmmI9O/fdjxB1RBwCA+LlSXe/pdWXFa7+srv/W60uaDlEHAID0xBxxR9QBACBCRirk4wZifhYSkl+4FyF6QNQBAKAjMZ+omK/ywp+oUDt9zzsGAwBxR9QBACAiMT8vpO4Tqi9V3EuaHlEHAIAwFCrmxYrX1ZntT5xtf/y8DHnEHVEHAICexPyZeuVNj6mN9Wr7ewBRBwAYHFaRDe1By/dJWP4dY0QAcUfUAQDgAjEXz3xVOPxIPeZnLf0O69798dJv4Tgcog4AAEYxX85k7yorfaS/bVVSHWfdEXUAgEFjzUKPQTCvLIl7bIYHIOoAANGL+bF7mZQWk/frs0WAuCPqAABZYq3+lkoSWuFsmflP1DgpGQKIOgBADmI+cauTzkqX5lnwa0vGSo73h6gDAIBZzI/0KjO531VlaMulewZEHQAgagpnO+ud656zT87AAnFH1AEAYhXzibt4j3loR78sSXWxJgQi6gAAA8RS/W3owvWOeu4YPIg6AEDSXujCEWKuKdzqrYkv3csqdRyHQ9QBAHoX89KR6X0RPkmEC8QdUQcACIk1+Ysz2e20ay4nBBB1AIBERAePcr12tpShLR0REEQdAKAFMa8Tu44Q86BYtzek7Z/QXIg6AMB5WPZ6edxoNxRu9RFBEhERdQCAxmKOgHSPpQwtfYOoAwCYxLx0hHpj6atVD8SpxV36apBRFEQdABDzsyHjOk6uLIn7efvugy1kg6gDAGL+qhg8cWSyp8KqpLrBiTuiDgBDoHAXVzKjRGn6/Ttx5yfVDaZ/EXUAGPJiT4JVXlgiMVmLO6IOAEMU89JRxCRnVu27Z1tfHlEHgCGJOZXfhkddqe7aEMYEog4AuYs5ld+gHiMX5VVkIe6IOgDkKuaDP7MMZ1Lvu4u4X8lN3BF1AEiRi44yle7lGXOA81i1717vuT9D1AEAuhdzisXAOuPqvH330iWUVImoA0DKYk6xGAhJ4c7fd09C3BF1AEhRzHlSGrTJRfvuUYs7og4AKYn5M/cykx2gbS7ad49S3BF1AEhBzJ+omJc0EfQ4Ns/ad4+qKiGiDgAxiznFYiA2Cnf2vnsU4o6oA0BsYs7DVSAFztt371XcEXUAiEXMebgKpMh5++69jGdEHQD6FnOhrK5DR5gd0uZvquvPqutSX+L+Gn0AAB2J+T/oX/FsJKz+6+r6uXuReFS4F8lwiDqkzEZ1/V91/av++zUd7+/o2Jdx32qFusv0AQB06JlzvhyGQOleHnmrs+ZlDkx1PrTmuSPqANCFmHO+HIZIHXpfFvdrbYo7og4AbYo59dgBXp0Lhc6TcRvijqgDQGgxXz6Sxh45wKuU7mVo/r3Q4o6oA0AoMRcBlwx2nl8OsBqZL/+uQv6OzqO1xR1RB4B1xbzUxekJzQLgzZfu1dD8e+uIO6IOAE3E/IrjkacAoSn1Gqm4/7WvuCPqAOAj5kKdxU6IHaAdzgrNm8QdUQcAi5jXiwwhdoDuOCs0f6G4I+oAcBbFkpjPHSF2gL4p3auh+X9yZ9R+QNQB4LSYi3cuxWJ+7gixA8RGHTWry8/Olrx5RB0AvhbzQsX8kOYAiJ7TofmJCD6iDgC19b+gGQCSpNRrdIm2AADHnjlAFvMYUQcAAMgERB0AAABRBwAAAEQdAAAAEHUAAABA1AEAABB1AAAAQNQBAAAAUQcAAABEHQAAAFEHAAAARB0AAAAQdQAAAEDUAQAAQLn8y+9f+13D927+8DfPDofceFXbTas/Dxu8dVG13QbDDwAgyFr8QfVnj3V3PU99h6FEGwAAQB6iPqmso2LAlqHc+4QhBAAAOYi6sDfgtttj+AAAQE6iPq081tEAvXS55ynDBwAAchL1oYrbTO8dAAAgG1EXhhiGJkEOAACyFPXil9+/NhlKg1X3Kl56wdABAIAcRX1onusWwwYAAHIW9ekQjrdV9zh2HGMDAIDMRV2YDaC92EsHAIDkRb0cuuDpMTaL4bJgWAEAQMyi/sDwmpEmkeXKnYBtBQAA0JuoHxhfl3MSmSUSMXe2qAYAAEA/ov7D3zw7VsFaxUSTybJCIxCWYjN46QAAEL2nLtwL6NHm6KUfVcbPgiEFAADRi3olWEfOlgQ2y6kevBbWsUQf7jGcAAAgFU9dsIaXZxm1kSVP4LgyeuYMJwAASEbUVbhKw0uzCMFrQR2LgYKXDgAAyXnqVm9d6sHn8PQ2q3FywFACAIAURd0qYEl76x7FZuZ6OgAAAKBXLvu+QQSsEry5QfDkeFtRvb5MtG0k0mBJ+Msu9K7HEi+69zLhfm3TCByvmDsLWgo64PXqenPFaz6jmRD100Jm8WLFW99NtG0sz4lf6KmA1AV8Ul23VZQK4/tOxL265P4/0bYYhGipgIvRd1PbbOLRZsfaZh/p3wWRHlhTwN+qrusq5Nc93vtVdX2uAv9bhH7Aoi5CVi1QC8NiJsfb9lNbtPQYm0Xckiw2o0K+pcJUrPFRhV7yOXvV50o/H1bXh1WfH2Yo5DNtt3UKLI103kyWPlvm0oeux62c6jfcNdyXnPLYTKzfpLzz+4aXypg9uOBzPljx/rLjEzBvL82/dQyC60uGwFdqqP9Kxb6rPlrVtvNAkcFx9V2Pmr65+g0bOXvqtaCtEvXao5m7tLDkA5SpHWPT5MUd197jY2vhE2Ou1DFykLInqicg9ly7xzRrkb+rW1v3eogASbTljmUMJWaw7RiFb3/F/18VuVt0sM6JCP9ABf1qS5//tl7iuX/ckfduadsQoj5yA3h0duNHr3ocb9tLqUF0Ebdk7j9I6J4kv+Fx9Y8POxzUtRg+FUs8tYJEMg6q6778ftdt3QX5rsfiUXRcclmE2mJ8bSXUh9ZIVJnA1pEI7U+q692WBP004r3/bXX9uLrecJC/qCuWJLFCw9mpYDVCoj/GJkJaXSLkEnLqqyb/aEncZykMAA3ZPnb9FlGaqLjf7cIg8ni+w1QN3xSwGiD7Ed/DVRXWiXrSXSPi/ndqTMAARH2ek3W/lAC18r5jDymrl/LUeD9dift99UBHkbZZoRGNu8528qEL7qi4d2EYZ/N8B4+IW50HEiOFCur1CH7Lu/pbXneQr6irsFkmxCwR635mXMxjtuzrpKeHEQnTaQ/0aWzRGzWCHvcY0Vi1uD8yJBSt662Xzvh8hwTmstXwOIzUQJe987+KTEQlDP8TRzg+a0/dR+ByWQgWsZ7R1nD7Y2dIeIrAa38USzhew+2xGkHLyAmDhy1HOiy5IqMEtlKsvy9GA10M3j+PtF3FyGCfPWdR97Duow7ZeSTV3Iv0958IZUBPs+7Xs65QRs19TUbrs93k++8G+rjjC9psEeg7pmoQtSLsmgCb9JaaGhyW9onRQBdBfzvA58iRNMlg/9S9yGL/WOet/LfnCHu+XA70Ofec4XibTLaIj4FZj7FFt/8WSNBFdOSstPm58EuPpX3fNc+ql60ZEZPtngR9HY9TFkkZDx85YxEZ3YaStrrt7FULTzNWYd9oKXQs83lVwqicqBhHWnzJanDEdoLl3TUEvT5jLtdn+u8XIQl4113zs+61sP/CdXimPYDRnXSxsM5EXYROzyUXhskWnagvVVVLzktfU9BLXdgaFXdQ8ZfrYOmJdjsNhKpzYV9T0GUMNzpLru0812tbvcqtBkZRm8I+d7ZTINLX24nO5djqTLztmmWYP1cv/NMG73uu77va0KCohf1nBiMiBo5SKSCzDpcCfpZF8CYdn70N6aVbj/ykIOhyL7vVAL9RXR+ECEHKZ8hnVf94w73Yp/QVmllX+7SaSNjku6T/pc22Q3moIiy60Gw4/22NsfZ/UHQ8WCJS0whPMli3+WLy0iWM/RcNPHMR839pIOhnCfxCP+u3DYT9Rw6yFPW5cSGPam/d42lsMWbJ3m8g6IcqTK2cs5c2UnG/5fz3ke+3nRWvhoNvIqEI+C0V87KldpPwfW0QeQl7S3kJpoQ5F8+RSZ8jqS4yA33i/LLcJdz9CxX1kDzXz/0vz/ddd5xjz0/UPY+3xWTdWxf4qLJk9XiTz4Iq/bMptbu7ME7Ucxfv0/eBPg/bOv6oUSLfpDgpc3urq73jJYPIp4+CRzk0d8RiwMRUMdKaozCPKEHuXeeXcPapa38fW2q//5vzC6l3VekOOvTUfYRvFlEbWJJqDmPKklVv1mcxFUHa6CPJTyMCmx4iNdIIRBtenO+xNfHMd3toM+mvG84vqeduC1tbqVWMTC30LiL4A09BX7hu9q8/U+PB57tiGQeIekjvzCV0vE29G4tXGE2CnIqTj+jVgt5b1qcaExsewj5podDKnrNn+dZRjXmPbXasbWbttzaMIev9b0UwL0RQLEZNTHXexbu1ht0/deGORVr53FPYJQz/FrKagKh7WuJW6z6GvbidxBYBX3GqBb33XAA1KnwyT3dCheF1/Prso2/EcHSxgbCPtZBOyO+3CHsMFSNTq/MuXro121zE9T97+p2fexoT7K3n5KkveWVl7N66h2UfzV66LpzWRbuMRdBPCbv1CJR4nqGKwvh8znZMZ6+XhN3aj3uBc1asoepZj/PCmuwaU513q6CLl/wfrt8jY7KWWJPyrjv21vMSdQ9vfdKzdb+V2CIgWMOrdfg4uprWGtK2Zt5P192v1S0W617zfozFkZaE3WoMhfTWF8ZIQZ8heKtBEdODmKyiLmL6PILf+7GzJ+f9KdKan6jPXcTH25YKpSSzCKi4WQVuN9JKX7VQ7Dp7SHnd7Grr+xeaeR5rm0l7WZP2dgJ767Fvqe0EvI8ueMPozcqZ8V9FNAytR90KB3mJegLH22aJLQI+ntBhxKV4l9k0vq5xRMejnv+xx+/pU9glwrHo2lvXuRylke7RxzHVeffx0mPiM2crTnPVURc+O09dsOxF91W8wrL4RHOW1SOycOz8z4X3JVCls+cr7LTYzydjNdLHb56FtX+3AvaV1UjvY0vNep8xGeiWDPHnKqKx8XHAe4SURD3W420eT3CKqYyk1fC5F+tjYc/hwOgBzhr0s4jLxPDSsq3qei3NKwnDzw0vDR0Ot4riTodzuTDOjZgexPS60Yv9daRD8DNn2+O/jrzm56lbF4Jxx8UrLHusR5EdY7Mm9R2kNPjUA7SMkVEDgbK+fj/BeWv9ze8HNiYsc2LWYTtYvysmA/1ND/GMFYvjQPg9R1H3ON7WSeasGg9FQK+kK2/Ekr19mFAIuYm37itQJkMokfyD0/OqNHrrobe2TPXgu3owj0dUICZj1yJ2cnwt5seZWvfVIUNP3SqQXRWvMD2NLbKFfhKwnWP11g8DtkN9btliCM0TnrsfGgV2HLCv5i6ShDmPbbR5ZMaupYJc7M8nt0YR2FfPVNStC8Gs5UWgMHousYnjbcNrypiPsAUSqMLD8LMK2YNUG0yjYJZ5NWlhPq9s/w4esbyVaB9bPPX/iXz4feVguKLu4Ym1HYK3eg+xeW+m0HvKg9AjickqFBYhO07cELL2++3A39l7wpxHEmRsuTHCtzMRTUsI/nUHWXrqgiWxp2hrL86jjOQ8wuxxi5B9lMFYXARqC+FmoO+LHUu/F4ENsNL1X4MitWIzQ4VkuVxF3eN4W1veugh6asfYfB6kc5TBWLQI1PeMn2URsk8yaDNLv7cRBu+tHnyidd4BsvPUrVbzpKW9OItlv4gwVOdjNA1BoKxe5zjQ98Xe76Z7CO0x9/zQpqlLM0EOIC9R72sh8CgjGWPS1CDEacmzyvn7huitFy3UoCD0DhCJp26daNPAnoX1menzCPvH0g5DEqeVAuExdjCG1uMg4PyzGuhjo4GyyCR6BRC9qM8Ni5B1z8yyCBTOliX7gKHQLwFDpeOOv2/I/WUxhKcBa1DgpQPEJOoex9tCWffWx24eMBQAvOksYc4jQS6mOu8A2XvqgvV42zTAImD5DBJqAJoZ6QvXXRloq2FA1A0Q9Y4XAlkEFh0sBPI8acv+KqG6vDAZaC2eoR4alvkTogZFinXeAQbhqVsXgnX34ixGwSKDqmJZiJMxS3plX3n05ziTOdx3/8+NhtTWmmPDshYQdQNEvY8vbft4m3oFlkUgdi+9r+NKsYrTccfflwKW/i9bnMvmB/KsYaSTIAcQuadunYBNS01avIIUEmqsoeQig7E4DtUextclbwxZ+72D411WMd1reI+W3JijDKJuAEmL+tzZjrdNPRcBWawnmVj11sU4B2/dUq/dWtr1KND3xc4k4Bhax1s/MrZ5kxoUM7x0gAREvcXjbaZnprsEnqWtHpbF67ydwVi0CJTVEzsK9H2xcztgm3XhrXsb6c4WdTuOtHgUwKA8dcFyvG1sLTWpoTqLZX+YUEKNyQNKeRBq/44CtYXVox918MzvGAyhTh5co6JqmVM7HuNC5nJheCmCDhCDqLdwvG1mfN1+Qn1kerxm4gJlzYEoAxpCPuMqVkPIIniLDn+WRVzHHvXgrf1D6B0gEk/dOiFnxqQg69PYyoT6yLoo76Q4AD2KBJnFSfd4LV7jLOG5aw1LLyKby6bf7lHi+ZA67wARibrH8bbZikVA/n92xWZ0UbYI1DTRgirWfvvQ83Mt+RqjAEVR+jCECqNBsuh4rJbG77ScarEaqVSQA4jMU7cK7VaARSDVutAmgXIvquil5qWbEhsb9JvVCNhLcDxYf/ODSOfySiPdUecdIGlRnxu80XNLTeoenWVPeT/RfrIK1E5iZ9bFCLH8Xu+FWxd7S4RDxlUyxpCHl37ch+CFKCyVa9QNYDCi7nG8bWsNL936HdHhsVDKQng3IXGyepxNF29zUZSEti7ut9xmIbBECC56aJM19D5nCQeI01O3etHfKDXpUXEq9brQ1kV6uu4T7jriofF169Tnty76Iw+x7NMQkojCJAHBs3731hn3KBG38QDmM3RPgah3642KJ7qweFUNrfrUQ3VzZy+Tej/mI27Vb7vr7FXw9tccU1aBmcYchtf+tEZh5n1mhOt3HxrbvBjofAZEPXtP3TpRv87y1r8zw3uSP/aiXolV4EYq7NGFlHW/1CqeiwBHsnyMgrseZ6i7bDPpx0fGl8s42U1kLr8i4pPvXLEeb6TOO3xjTHgYx4h6h8Jl2TteFvKpG1BCTdU+B86vHvyjmIRdBdMnzL0dyGv0EfaHMU38JUG39uO9GMLSaoxZxurXRvnfv/H7M0eCHDTDOuYR9Ugt/Nq6tyRalR0X4GgbH6GLRthVKB96vGU/YHTFxxgaaZuNI2izWtCtv0U82A8Sm8ujn/7R1RPv/E++/Rp13qEp1vn9PqLePXNnO94mi1dhEYecOkwNlIOUhF1D7j7eZlBxUs/VxxjqXdgbCHqQyEYLc3klf/mdKzd//Ie/V1x97RJeOjSd46XLu0hXuqLucbzN4qXnatWLoeKzpyjC8LgPkdLks/segi79v9mSMeRj4I20ze700GbST089BX03tn1mncsr598fv/5asf3dP/hRSEMBBsnC+Lo7OTfCpUh/VyjvOkurfsnz9Nk7LboUKbGGq0vC7b7n5rfbSmpU73/h+TZJnnvYlXWvEajHHkbQidBpvkWMmObgm5cvXTO8jDrvcBEfGV+3l3PCXJSi7nG8bbBWvXplTTxaEanHbWZ5q+EgnqbvefndDqqgbTr/Z4zLfTxVwW2rzaQGg4i5b9lauZfdyMdpqAgCoXe40OjzeO39XMPwlyL+betO4HnuVr2GlJvso9b77I9CPdBEPXN5UMdT9c5HDfrroIM2q8P7vhniI7XwT8Q91IKgYi57577757WgbyRQhCWEGOeW8ArtOINWYY/udFD2ou5RGvU8HgxkIM9d8wSpiVqsX1SX/J361I6X16qQS5j9C/di77xo8DtE0Lc7nvwbDYTd6f2JN/2FhuXv+IbyVMjvqgH0yNmrxKUo6LUHte7vxEuH0OPkJHclxtoU63A5gQ5qUsv8aEhWvQh7NTCda17qtD77P1PROXYvQ6an96lu6uvHDbzx3gV9qc2OqvvccH5Z+aeZ6uW0/RdLgvu/S6/7nhoDhQtT1SolQT+JjlTtI8Zn03wOU8IdgKz71Vg7dPatv/qki8zdezk89S92UZ+rV+S76N4b4GCuhb1J6PusgT5Z8ubbYrfPBC8V9huuWej7vMhH220mi89mgnXP760h6ofUeQcPtnUOjjzn7kQdGpljnywb6SmNv5j31H2Ot71i1Q+1OIXet3ifZeQ/9ViF6SCSMbaRiCd4UP3ejRQFbs3kV0Lv0GRON3VopupM1rkustX2O0Q9HPssAH7eZ/Xnlov3MbOysN+KKcwli4BuAey69fd+2zSCdhMfnk3yXBbUeYeG6+D2EO/9UgKd42vhHzCgT0RKMrw3I/LaTx40op5mGWm7HahBtIjoZ4nxcyOHvT6NJPkaTQ8cQPPxditSQ33QnrqP980zll8d1Ic6qPd7Hthz9c4PEmgzOTolobu+tzHqZLjNzMa0TySNOu8QwmO/4eKNXA5T1D2Ot7H3drbX/oEO7P2OhWquXuZ2ajUDJIu2uqTNtjv23OW7pL1uZXqCw0ekmc8Qag3cVEN9kfv9Xk7ot6463sbe24qBXf0RcZfCKZIIIk/Emrgwx9JOe5gSMs0iaqKe4lzPokubSdsVgb+mVE/iQe5jWIw7jyNHMXnp+4Y+hIjbVo3khc7lHWd/dHdSfIvxNGy08IJct12zs+d1GdCP1LAqB9Bm41Nt5ivyx0ttdogxCtDrXBZxv7lqLlfz1KKX/6h//xlRh9iEvmZy6n8vamFCjL6xONQG0WnjqKy9DUqdAkQ/l4tT4n4yn42Pg+5d1AEAACAMb+kFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQkv8XYADtibnXA/oypwAAAABJRU5ErkJggg==',
                content_id: 'moovInitLogoId',
                disposition : 'inline'
            }],
            'template_id': _config.sendgridTemplates.forgotPassword
        };

        // Sending email
        _logger.debug('[Forgot Password] Sending email to user');
        await _sendGrid.sendEmail(message);
        res.sendStatus(200);
    } catch (error) {
        error.message = `[Forgot Password] ${error.message}`;
        _logger.error(error);
        res.sendStatus(200);
    }
};

const login = async (req, res) => {
    try {
        // Extracting data
        const { email, password } = req.body;

        // Check if user exists
        _logger.debug('[Login] Checking if user exists');
        const user = await users.findOne({ where: { email }, raw: true });
        if (!user || user.length < 1) {
            throw new Error('Incorrect username or password.');
        }

        // Compares password
        _logger.debug('[Login] Comparing password');
        const isCorrectPwd = await _bcrypt.compare(password, user.password);
        if (isCorrectPwd) {
            res.status(200).json({
                user: {
                    username: user.username,
                    email: user.email
                },
                token: signToken(user, oneWeek)
            });
        } else {
            throw new Error('Incorrect username or password.');
        }
    } catch (error) {
        error.message = `[Login] ${error.message}`;
        _logger.error(error);
        res.status(401).json({ error: 'Incorrect username or password.' });
    }
};

const resetPassword = async (req, res) => {
    try {
        // Checks if email exists
        _logger.debug('[Reset Password] Checking if email exists');
        const { email, pin, newPassword } = req.body;
        if (!email) {
            throw new Error('No email was provided');
        }

        // Checks if pin was provided
        _logger.debug('[Reset Password] Checking if pin was provided');
        if (!pin) {
            throw new Error('No pin was provided');
        }

        // Finds user with email
        _logger.debug('[Reset Password] Finding user');
        const user = await users.findOne({ where: { email }, raw: true });
        if (!user || user.length < 1) {
            throw new Error('The email doesn\'t exists');
        }

        // Verify pin
        _logger.debug('[Reset Password] Verifying pin');
        const token = await _tokensController.getToken(user.id, 'pin', undefined);
        if (!token) {
            throw new Error(`Token not found for user ${user.id}`);
        } else if (token.token !== pin) {
            throw new Error('Incorrect pin');
        }

        // Removes tokens from user
        _logger.debug('[Reset Password] Removing tokens for user');
        _tokensController.destroyTokens(user.id);

        // Change password
        _logger.debug('[Reset Password] Replacing password');
        const hashedPassword = await hashPassword(newPassword);
        users.update({ password: hashedPassword }, { where: { id: user.id } });

        res.sendStatus(200);
    } catch (error) {
        error.message = `[Reset Password] ${error.message}`;
        _logger.error(error);
        res.status(500).json({ error: 'Could not reset password' });
    }
};

const signup = async (req, res) => {
    try {
        const hashedPassword = await hashPassword(req.body.password);

        // Generates user
        const payload = {
            id: _random.generateModelRandomId('users'),
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword
        };

        // Creates user
        const user = await users.create(payload);

        // Logins user
        return res.status(200).json({
            user: {
                username: user.username,
                email: user.email
            },
            token: signToken(user.get({ plain: true }), oneWeek)
        });
    } catch (error) {
        error.message = `[Signup] ${error.message}`;
        _logger.error(error);
        res.status(500).json({ error: 'Could not create user.' });
    }
};

export default {
    forgotPassword,
    getUser,
    login,
    resetPassword,
    signup,
};
