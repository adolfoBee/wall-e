import _db from '../../models';
import _config from '../../config/config';
import _random from '../../utils/random';
import _logger from '../../utils/logger';
import _usersController from './users';

const Op = _db.Sequelize.Op;
const { tokens } = _db;
const tokenTypes = _config.tokenTypes;

async function tokenPicker(tokenType) {
    let selectedToken;
    tokenTypes.forEach(token => {
        if (token.name === tokenType) {
            selectedToken = token;
        }
    });
    return selectedToken;
}

const tokensController = {
    async generateToken(userId: string, tokenType: string): Promise<any> {
        // Retrieve token if exists
        _logger.debug('[Generate Token] Retrieving token if exists');
        const token = await this.getToken(userId, tokenType, undefined);

        // If token exists return it
        if(token) {
            return token;
        }

        // Selects type of token
        _logger.debug('[Generate Token] Selects the type of token');
        const selectedToken = await tokenPicker(tokenType);

        let tokenHash;
        if (selectedToken.name === 'pin') {
            // Generates new reset password pin
            tokenHash = await _random.generatePin();
        } else if (selectedToken.name === 'confirmLink') {
            // Generates new token for link
            tokenHash = await _random.generateToken(selectedToken.length);
        }

        // Checks that tokenHash is not empty
        _logger.debug('[Generate Token] Checking that a hash has been generated');
        if (!tokenHash) {
            throw new Error('No token hash was generated');
        }

        // Generates token Data
        const newTokenData = {
            userId,
            tokenType,
            token: tokenHash,
            tokenExpirity: selectedToken.expirity
        };

        // Stores token in the database
        _logger.debug('[Generate Token] Storing token in the DB');
        const newToken = await tokens.create(newTokenData);
        if (!newToken || newToken.length < 1) {
            throw new Error(`Couldn't create token for user ${userId}`);
        }

        return newToken;
    },

    async getToken(userId: string, type: string, token?: string) {
        const whereClause = {};

        if (userId && userId !== '') {
            whereClause['userId'] = userId;
        }

        if (type && type !== '') {
            whereClause['tokenType'] = type;
        }

        if (token && token !== '') {
            whereClause['token'] === token;
        }

        whereClause['tokenExpirity'] = { [Op.gt]: new Date() };

        return await tokens.findOne({
            where: whereClause,
            raw: true
        });
    },

    async destroyTokens(userId: string) {
        return await tokens.destroy({
            where: { userId },
            raw: true
        });
    },

    async generateConfirmationToken(req, res) {
        try {
            // Checking if data was sent
            _logger.debug('[Generate Confirmation Token] Checking if correct data was sent');
            const { userId, tokenType } = req.body;
            if (!userId || userId === '') {
                throw new Error('No user id was provided');
            } else if (!tokenType || tokenType === '') {
                throw new Error('No token type was provided');
            } else if (tokenType !== 'confirmLink') {
                throw new Error('Wrong token type was sent');
            }

            // Generate random token
            _logger.debug('[Generate Confirmation Token] Generating random token');
            const token = await tokensController.generateToken(userId, tokenType);
            if (!token) {
                throw new Error('There was an error generating the token');
            }

            return res.status(200).json({ token: token.token });
        } catch (error) {
            _logger.error(error);
            return res.status(500).json({ error: 'There was an error generating the token'});
        }
    },

    async verifyToken(req, res) {
        try {
            // Checking if data was sent
            _logger.debug('[Verify Token] Checking if token was sent');
            const { token, userId, tokenType } = req.body;
            if (!token || token === '') {
                throw new Error('No token was provided');
            } else if (!userId || userId === '') {
                throw new Error('No user id was provided');
            } else if (!tokenType || tokenType === '') {
                throw new Error('No token type was provided');
            }

            // // Retrieve token if exists
            _logger.debug('[Verify Token] Retrieving token data');
            const tokenData = await tokensController.getToken(userId, tokenType, token);

            // Retrieve user with id
            _logger.debug('[Verify Token] Retrieving user with id');
            const user = await _usersController.getUser(userId);
            if (!user || user.length < 1) {
                throw new Error('The email doesn\'t exists');
            }

            // Verifies the token
            if (tokenData.token === token) {
                // Deletes existing token
                _logger.debug('[Verify Token] Deleting token');
                tokensController.destroyTokens(user.id);

                res.sendStatus(200);
            } else {
                res.sendStatus(404);
            }

        } catch (error) {
            error.message = `[Verify Token] ${error.message}`;
            _logger.error(error);
            res.status(500).json({ error: 'Could not reset password' });
        }
    }
};

export default tokensController;