const tablePrefixes = Object.freeze({
    users: 'US',
    tokens: 'TO'
});

export default tablePrefixes;
