import _discord from './discord';

import { createLogger } from '@bahatron/logger';

const logger = createLogger({
    debug: true, // up to you
    colours: true, // up to you,
    formatter: process.env.NODE_ENV === 'production' ?
    (context) => JSON.stringify(context) : undefined
});

logger.on('warning', (params) => {
    _discord.warning(`${params.message} 
        ${params.context && params.context.name ? params.context.name : ''}
    `);
});

logger.on('error', (params) => {
    _discord.error(`${params.message} 
        ${params.context && params.context.name ? params.context.name : ''}
    `);
});

export default logger;
