import crypto from 'crypto';
import tablePrefixes from '../constants/table-prefixes';

function randomAlphaNum(len): string {
    return crypto
        .randomBytes(Math.ceil((len * 3) / 4))
        .toString('base64')
        .slice(0, len)
        .toUpperCase()
        .replace(/\+/g, '0')
        .replace(/\//g, '0');
}

function generateModelRandomId(tableName): string {
    return `${tablePrefixes[tableName]}_${randomAlphaNum(15)}`;
}

function generateToken(len) {
    return crypto.randomBytes(len).toString('hex');
}

function generatePin() {
    return Math.floor((Math.random() * 999999) + 1);
}

export default {
    randomAlphaNum,
    generateModelRandomId,
    generateToken,
    generatePin
};
