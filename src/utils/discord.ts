import _axios from './axios';
import _moment from 'moment';

const WEBHOOK = process.env.DISCORD_WEBHOOK || '';

function formatContext(context: Record<string, string>): string {
    return Object.entries(context).reduce((result: string, [key, value]) => {
        return result.concat(`${key}: ${value.toString()}\n`);
    }, '');
}

async function execute({
    message,
    color,
    description = {},
}: {
    message: string;
    color: number;
    description?: any;
}) {
    if (!WEBHOOK) {
        return;
    }

    await _axios.post(WEBHOOK, {
        embeds: [
            {
                title: message,
                color,
                description: formatContext(description),
                timestamp: _moment().format(),
            },
        ],
    });
}

const discord = {
    async info(message: string, context?: Record<string, string>) {
        return execute({
            message: `[INFO] - ${message}`,
            color: 3694847,
            description: context,
        });
    },

    async warning(message: string, context?: Record<string, string>) {
        return execute({
            message: `[WARN] - ${message}`,
            color: 15832064,
            description: context,
        });
    },

    async error(message: string, context?: Record<string, string>) {
        return execute({
            message: `[ERROR] - ${message}`,
            color: 12779520,
            description: context,
        });
    },
};

export default discord;
