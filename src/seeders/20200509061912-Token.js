'use strict';

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.bulkInsert(
            'tokens',
            [
                {
                    id: 'mockPinId',
                    user_id: 'adolfoId',
                    token_type: 'pin',
                    token: 'mockPin',
                    token_expirity: new Date(Date.now() + 30*60000), // 30 minutes
                    createdAt: new Date(),
                    updatedAt: new Date()
                },
                {
                    id: 'mockTokenId',
                    user_id: 'adolfoId',
                    token_type: 'confirmLink',
                    token: 'mockToken',
                    token_expirity: new Date(Date.now() + 30*60000), // 30 minutes
                    createdAt: new Date(),
                    updatedAt: new Date()
                }
            ],
            {}
        ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('tokens', null, {})
};
