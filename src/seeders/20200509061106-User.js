'use strict';

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.bulkInsert(
            'users',
            [
                {
                    id: 'simonId',
                    username: 'simon',
                    email: 'simon@simon.com',
                    password: '',
                    createdAt: new Date(),
                    updatedAt: new Date()
                },
                {
                    id: 'adolfoId',
                    username: 'adolfo',
                    email: 'adolfo@adolfo.com',
                    password: '',
                    createdAt: new Date(),
                    updatedAt: new Date()
                }
            ],
        ),

    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('users', null, {})
};
