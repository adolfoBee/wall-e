import _sendGridClient from '@sendgrid/mail';

_sendGridClient.setApiKey(process.env.SENDGRID_API_KEY || '');

const sendGridClient = {
    async sendEmail(message) {
        try {
            if (process.env.NODE_ENV === 'production') {
                await _sendGridClient.send(message);
            }
        } catch (error) {
            throw new Error(`[SendGrid]: Couldn't send the email.
                Email: ${message.to}
                Code: ${error.code}
                Reason: ${error.message}
                Extra: ${error.response.body.errors[0].message}
            `);
        }
    }
};

export default sendGridClient;
