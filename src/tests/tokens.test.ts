import _request from 'supertest';
const url = 'http://localhost:3000';

describe('Tokens Endpoints', () => {
    it('should generate a new confirmation token', async () => {
        const body = {
            userId: 'adolfoId',
            tokenType: 'confirmLink'
        };

        const res = await _request(url)
        .post('/tokens/generateConfirmationToken')
        .send(body);

        expect(res.body.token).toBeDefined();
        expect(res.statusCode).toEqual(200);
    });

    it('should verify the token', async () => {
        const body = {
            token: 'mockToken',
            userId: 'adolfoId',
            tokenType: 'confirmLink'
        };

        const res = await _request(url)
        .post('/tokens/verifyToken')
        .send(body);

        expect(res.statusCode).toEqual(200);
    });
});
