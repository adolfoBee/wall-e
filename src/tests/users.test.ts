import _request from 'supertest';
import _db from '../models';
const url = 'http://localhost:3000';
const Op = _db.Sequelize.Op;
const { users, tokens } = _db;

describe('Users Endpoints', () => {
    it('should create a new user', async () => {
        const body = {
            username: 'beedo',
            password: 'beedo',
            email: 'beedo@beedo.com'
        };

        const res = await _request(url)
        .post('/users/signup')
        .send(body);

        expect(res.body.user.username).toEqual('beedo');
        expect(res.body.user.email).toEqual('beedo@beedo.com');
        expect(res.statusCode).toEqual(200);

        const email = res.body.user.email;

        const user = await users.findOne({ where: { email }, raw: true });
        if (!user || user.length < 1) {
            throw new Error('Incorrect username or password.');
        }

        expect(user).toBeDefined();
        expect(user.email).toEqual('beedo@beedo.com');
        expect(user.username).toEqual('beedo');
    });

    it('should login with the new user', async () => {
        const body = {
            email: 'beedo@beedo.com',
            password: 'beedo',
        };

        const res = await _request(url)
        .post('/users/login')
        .send(body);

        expect(res.body.user.username).toEqual('beedo');
        expect(res.body.user.email).toEqual('beedo@beedo.com');
        expect(res.statusCode).toEqual(200);
    });

    it('should ask for a pin using forgot password', async () => {
        const body = {
            email: 'simon@simon.com',
        };

        const res = await _request(url)
        .post('/users/forgotPassword')
        .send(body);

        expect(res.statusCode).toEqual(200);
    });

    it('should reset user password', async () => {
        const pin = await tokens.findOne({
            where: {
                userId: 'simonId',
                tokenType: 'pin',
                tokenExpirity: { [Op.gt]: new Date() }
            },
            raw: true
        });

        const body = {
            email: 'simon@simon.com',
            pin: pin.token,
            newPassword: 'newMockedPass'
        };

        const res = await _request(url)
        .post('/users/resetPassword')
        .send(body);

        expect(res.statusCode).toEqual(200);
    });
});
